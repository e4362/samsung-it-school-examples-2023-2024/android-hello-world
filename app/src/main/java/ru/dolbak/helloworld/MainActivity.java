package ru.dolbak.helloworld;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    EditText editText1, editText2;
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editText1 = findViewById(R.id.editTextNumber3);
        editText2 = findViewById(R.id.editTextNumber4);
        textView = findViewById(R.id.textView);
    }

    public void sumNumbers(View view) {
        int a = Integer.parseInt(editText1.getText().toString());
        int b = Integer.parseInt(editText2.getText().toString());
        int c = a + b;
        textView.setText(String.valueOf(c));
    }
}